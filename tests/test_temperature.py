import os.path
import pytest
from climate.temperature import DEFAULT_TEMPERATURE_DATA_PATH, Temperatures

TEST_DATA_FILE_PATH = os.path.join(
    DEFAULT_TEMPERATURE_DATA_PATH,
    'temperature_by_country_light.csv',
)


def test_temperature_data_loading():
    """Test temperature data loading"""

    t = Temperatures(data_file_path=TEST_DATA_FILE_PATH)

    assert t.data is not None


def test_set_country():
    """Test the _set_country() private method"""

    # Country exists
    t = Temperatures(data_file_path=TEST_DATA_FILE_PATH)
    t._set_country('France')
    assert t.country == 'France'

    # Country does not exists
    with pytest.raises(KeyError):
        t._set_country('Utopia')
